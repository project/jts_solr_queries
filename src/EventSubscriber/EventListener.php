<?php

namespace Drupal\jts_solr_queries\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\search_api\Event\GatheringPluginInfoEvent;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\search_api\Event\SearchApiEvents;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Class EventListener.
 */
class EventListener implements EventSubscriberInterface {

  use StringTranslationTrait;

  /**
   * Drupal\Core\Messenger\MessengerInterface definition.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructs a new EventListener object.
   */
  public function __construct(MessengerInterface $messenger) {
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[SearchApiEvents::GATHERING_DATA_TYPES] = ['dataTypeInfoAlter'];

    return $events;
  }

  /**
   * Method called when SearchApiEvents::GATHERING_DATA_SOURCES is dispatched.
   *
   * @param \Drupal\search_api\Event\GatheringPluginInfoEvent $event
   *   The  type info alter event.
   */
  public function dataTypeInfoAlter(GatheringPluginInfoEvent $event) {
    $dataTypePluginInfo = &$event->getDefinitions();
    if (isset($dataTypePluginInfo['rpt'])) {
      // OVERRIDE RPT with RPT_SHAPES.
      $dataTypePluginInfo['rpt']['class'] = 'Drupal\jts_solr_queries\RptShapesDataType';
      $dataTypePluginInfo['rpt']['provider'] = 'jts_solr_queries';
      $dataTypePluginInfo['rpt']['description'] = $this->t("Unfiltered Spatial Recursive Preﬁx Tree data type implementation. To allow shapes. Requires geofield as input");
      // $dataTypePluginInfo['rpt']['label'] ="notused";
      // $dataTypePluginInfo['rpt']['id'] ="rpt_old";
    }
    // \Drupal::messenger()->addMessage('Event SearchApiEvents::GATHERING_DATA_TYPES thrown by Subscriber in module jts_solr_queries.', 'status', TRUE);
  }

}
