<?php

namespace Drupal\jts_solr_queries\Plugin\views\filter;

use Drupal\geofield\Plugin\views\filter\GeofieldProximityFilter;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\Link;

/**
 * Defines a filter for filtering on RPT fields using JTS.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("search_api_jts_filter")
 */
class SearchApiJtsFilter extends GeofieldProximityFilter {

  /**
   * Display the filter on the administrative summary.
   */
  public function adminSummary() {
    $output = parent::adminSummary();
    return $this->operator . ' jts ' . $output;
  }

  /**
   * {@inheritdoc}
   */
  public function operators() {
    $operators = [
      "contains" => [
        'title' => $this->t('Contains'),
        'method' => 'jtsOp',
        'short' => 'Contains',
        'values' => 0,
      ],
      // TODO add this note whenever all predicates are implemented...
      /*"is within" => [
        'title' => t('Is within'),
        'method' => 'jtsOp',
        'short' => 'IsWithin',
        'values' => 1,
      ],
      "is disjoint to" => [
        'title' => t('Is disjoint to'),
        'method' => 'jtsOp',
        'short' => 'IsDisjointTo',
        'values' => 0,
      ],*/
    ];

    return $operators;
  }

  /**
   * {@inheritdoc}
   */
  public function valueForm(&$form, FormStateInterface $form_state) {
    // We could use a try /except block if geoPHP raised an error here, it
    // will help to set default lat and lon as non numeric
    parent::valueForm($form, $form_state);
    if ($this->operators()[$this->operator]['values'] == 0) {
      // Disable the inputs, as we only want  to enter coords.
      $form['value']['value']['#access'] = FALSE;
      $form['value']['min']['#access'] = FALSE;
      $form['value']['max']['#access'] = FALSE;
      if ($form_state->get('exposed')) {
        if (isset($form['value']['source_configuration']['origin_summary']['#value'])) {
          // Override the summary, as we don't need to summarize distance, but we inherit the
          // work done by source plugin...
          $summary_string_args = $form['value']['source_configuration']['origin_summary']['#value']->getArguments();
          $form['value']['source_configuration']['origin_summary']['#value'] = $this->t('@operator Latitude: @lat and Longitude: @lon.', [
            "@operator" => $this->operator,
            '@lat' => $summary_string_args["@lat"],
            '@lon' => $summary_string_args["@lon"],
          ]);
        }
      }
    }
    if (!$form_state->get('exposed')) {
      $spatial_info_url = Url::fromUri("https://cwiki.apache.org/confluence/display/solr/SolrAdaptersForLuceneSpatial4#SolrAdaptersForLuceneSpatial4-SpatialPredicates",
        [
          "attributes" => [
            "target" => "_blank",
          ],
        ]
      );
      // TODO add this note whenever all predicates are implemented...
      /*$form['operator']['#description'] = $this->t("If you only have indexed points, then don't use IsWithin or Contains; keep it to Intersects (or IsDisjointTo if you need that).</br>
      If you have indexed non-point shapes, then you might want to search according to the \"IsWithin\" and \"Contains\" predicates relative to your query shape. To clarify, use \"IsWithin\" if you want to search for indexed shapes that are WITHIN the query shape. Vice-versa for Contains.</br>
      The semantics of IsDisjointTo is defined as the inverse of Intersects, but with the constraint that the field must have spatial data. No spatial predicate will match a document that has no spatial data.</br>
      More information about @spatial_predicates_url.
      ", ['@spatial_predicates_url' => Link::fromTextAndUrl($this->t('spatial predicates'), $spatial_info_url)->toString()]);*/
      $form['operator']['#description'] = $this->t("More information about @spatial_predicates_url.", ['@spatial_predicates_url' => Link::fromTextAndUrl($this->t('spatial predicates'), $spatial_info_url)->toString()]);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    try {
      /** @var \Drupal\geofield\Plugin\GeofieldProximitySourceInterface $source_plugin */
      $this->sourcePlugin = $this->proximitySourceManager->createInstance($this->options['source'], $this->options['source_configuration']);
      $this->sourcePlugin->setViewHandler($this);
      $this->sourcePlugin->setUnits($this->options['units']);
      $info = $this->operators();
      // Add query condition in case of valid proximity filter options.
      if ($haversine_options = $this->getHaversineOptions()) {
        $this->{$info[$this->operator]['method']}($haversine_options);
      }
      // Otherwise output empty result in case of unexposed proximity filter.
      elseif (!$this->isExposed()) {
        // Origin is not valid so return no results (if not exposed filter).
        drupal_set_message($this->t('The location %location could not be resolved and was ignored.', ['%location' => $this->value['value']]), 'warning');
      }
    }
    catch (\Exception $e) {
      watchdog_exception('jts_solr_queries', $e);
    }
  }

  /**
   * Will create the spatial query.
   *
   * This operation is the basis of the plugin, it will create a query,
   * packing as option with the standard solr_param_fq to rptm field.
   * While there's no special logic in serarch api solr. Despite
   * search_api_location has a lot of work done in the spatial querying, the
   * JTS is not included so that's why this way of querying and not using the
   * search_api_location prepared fields.
   * To illustrate, the final (solr_param_fq)field query must be something like:
   *
   * rptm_specified_field:"Intersects(41.23,2.19)"
   *
   * And it could be passed to query using $query->setOption.
   *
   * $query->setOption("solr_param_fq", "rptm_specified_field:\"Int"
   *
   * This will work only with solr, as is the only one with spatial queries.
   *
   * @param array $options
   *   An associative array with the option such as field, lat and lon.
   */
  protected function jtsOp(array $options) {
    // empty cannot reach here, as it will raise geophp exception
    // before so, aside of !empty we use the var as 0 will mean 
    // not defined anyway nobody could set 0 without floatin
    if (
      !empty($options["lat"]) &&
      !empty($options["lon"]) &&
      $options["lat"] !== 0 &&
      $options["lon"] !== 0
    ) {
      // We are not using $this->value['value'] here... @todo whenever
      // predicates are used.
      /** @var \Drupal\views\Plugin\views\query\Sql $query */
      $query = $this->query;
      $predicate = $this->operators()[$this->operator]['short'];
      $query->setOption("solr_param_fq", "rptm_" . $options["field"] . ":\"$predicate(" . $options["lat"] . "," . $options["lon"] . ")\"");
    }
  }

  /**
   * Gets the haversine options.
   *
   * @return array
   *   The haversine options.
   *
   * @throws \Drupal\geofield\Exception\HaversineUnavailableException;
   */
  protected function getHaversineOptions() {

    // Lan and lon.
    $origin = $this->sourcePlugin->getOrigin();
    if (empty($origin['lat']) && empty($origin['lon'])) {
      return NULL;
    }
    if (!$origin || (!is_numeric($origin['lat']) && !is_numeric($origin['lon']))) {
      throw new HaversineUnavailableException('Not able to calculate Haversine Options due to invalid Proximity origin location.');
    }

    $haversine_options = [
      'field' => $this->realField,
      'lat' => $origin['lat'],
      'lon' => $origin['lon'],
    ];

    return $haversine_options;
  }

  /**
   * Get the radius in specified units.
   *
   * @param float $quantity
   *   The quantity of the configured units to be multiplied by its unit factor.
   *
   * @return string
   *   The radius in specified units.
   */
  protected function convertRadius($quantity) {
    // Will use this whenever all the predicates are implemented
    // If the radius isn't numeric omit it. Necessary since "no radius" is "-".
    $radius = (!is_numeric($quantity)) ? NULL : $quantity;
    $multiplier = $this->getUnits()[$this->options['units']]['multiplier'];
    $radius *= $multiplier;

    return $radius;
  }

  /**
   * Returns an array of distance units.
   *
   * Distance searching is kilometer based, so all multipliers must be relative
   * to 1 kilometer.
   *
   * @return array
   *   An associative array with supported distance units and their
   *   specifications.
   */
  protected function getUnits() {
    return [
      'GEOFIELD_KILOMETERS' => [
        'id' => 'km',
        'multiplier' => 1,
        'label' => $this->t('Kilometers'),
        'abbreviation' => 'km',
      ],
      'GEOFIELD_MILES' => [
        'id' => 'mi',
        'multiplier' => 1.60935,
        'label' => $this->t('Miles'),
        'abbreviation' => 'mi',
      ],
      'GEOFIELD_YARDS' => [
        'id' => 'yd',
        'multiplier' => 0.0009144,
        'label' => $this->t('Yards'),
        'abbreviation' => 'yd',
      ],
      'GEOFIELD_FEET' => [
        'id' => 'ft',
        'multiplier' => 0.0003048,
        'label' => $this->t('Feet'),
        'abbreviation' => 'ft',
      ],
      'GEOFIELD_NAUTICAL_MILES' => [
        'id' => 'nmi',
        'multiplier' => 1.852,
        'label' => $this->t('Nautical miles'),
        'abbreviation' => 'nmi',
      ],
      'GEOFIELD_METERS' => [
        'id' => 'm',
        'multiplier' => 0.001,
        'label' => $this->t('Meters'),
        'abbreviation' => 'm',
      ],

    ];
  }
}
