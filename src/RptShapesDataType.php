<?php

namespace Drupal\jts_solr_queries;

use Drupal\search_api_location\Plugin\search_api\data_type\LocationDataType;

/**
 * Provides the rpt unfiltered data type.
 *
 * @SearchApiDataType(
 *   id = "rpt",
 *   label = @Translation("Unfiltered Spatial Recursive Preﬁx Tree"),
 *   description = @Translation("Unfiltered Spatial Recursive Preﬁx Tree data type implementation. To allow shapes. Requires geofield as input")
 * )
 */
class RptShapesDataType extends LocationDataType {

  /**
   * {@inheritdoc}
   */
  public function getValue($value) {
    $geom = \geoPHP::load($value);

    if ($geom) {
      return $geom->out('wkt');
    }
    else {
      return $value;
    }
  }

}
