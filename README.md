This module depends on search_api_location feature request to allow indexing
polygons -> https://www.drupal.org/project/search_api_location/issues/3113266

These are the features included in module:

* Override of the **rpt datatype** that restricts the solr indexing to lat,lon
  to allow indexing shapes. There's a subscription to datatypes event
  dispatched by search_api to overwrite the RPT datatype Class with the
  class provided by this module that is not filtering to lat,lon pair
  and allow more shapes.
* It provides a **filter plugin for views**. To search if user input point is
  being contained  by the indexed polygons. (more predicates may be done, but
  not by now, look at possibilities in   https://cwiki.apache.org/confluence/display/solr/SolrAdaptersForLuceneSpatial4#SolrAdaptersForLuceneSpatial4-SpatialPredicates)
* Also it adds the **configuration to do spatial queries** to the schema.xml
  configuration that generates search_api_solr. Replacing:
  ```
  <fieldType name="location_rpt" class="solr.SpatialRecursivePrefixTreeFieldType" geo="true" distErrPct="0.025" maxDistErr="0.001" distanceUnits="kilometers"/>
  ```
  with
  ```
    <fieldType name="location_rpt" class="solr.SpatialRecursivePrefixTreeFieldType"
            spatialContextFactory="org.locationtech.spatial4j.context.jts.JtsSpatialContextFactory"
        autoIndex="true"
        validationRule="repairBuffer0"
        distErrPct="0.025" maxDistErr="0.001" distanceUnits="kilometers" />
  ```

* TODO: Tests: With this module we can see how alter a search api query with
  jts_solr_queries_search_api_solr_converted_query_alter that modifies the select
  of a given search. It's the behavior that must be in the tests that must
  be done, search if there are any polygons indexed in a specified index that
  contains a given point, in code 'events' index is used.
